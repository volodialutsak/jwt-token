const {Router} = require('express');
const {findUser, generateToken} = require('../services/loginService');

const router = Router();

// @desc   Login user, generating token
// @route  POST /api/login
router.post('/', (req, res) => {
	const user = findUser(req.body);
	const token = generateToken(user);
	const {id} = user;

	res.status(200).json({id, token});
})

module.exports = router;