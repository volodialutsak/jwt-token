const {Router} = require('express');
const {getUsernames, parseToken, getUserData} = require('../services/userService');

const router = Router();

// @desc   Get usernames of Users
// @route  GET /api/users
// @access Unprotected
router.get('/', (req, res) => {
	const usernames = getUsernames();	

	res.status(200).json({usernames});
})

// @desc   Get user data 
// @route  GET /api/users
// @access Protected by JWT Token
router.get('/:id', (req, res) => {
	const {id} = req.params;
	const token = parseToken(req.headers.authorization);
	const userData = getUserData(id, token);
	
	res.status(200).json(userData);
})

module.exports = router;