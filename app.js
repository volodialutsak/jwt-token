const express = require('express');
const morgan = require('morgan');
const apiErrorHandler = require('./middleware/errorHandler');
const dotenv = require('dotenv');

dotenv.config();
const port = process.env.PORT || 8080;

const app = express();

// Parsers
app.use(express.urlencoded({extended: false}));
app.use(express.json());

// Logging
app.use(morgan('dev'));

// Routes
app.use('/api/login', require('./routes/loginRouter'));
app.use('/api/users', require('./routes/usersRouter'));

// Error handler
app.use(apiErrorHandler);


app.listen(port, (req, res) => {
  console.log(`
    ################################################
          Server listening on port: ${port}
    ################################################
  `);
}).on('error', err => {
  console.error(err);
  process.exit(1);
});