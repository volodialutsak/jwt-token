const jwt = require(`jsonwebtoken`);
const users = require(`../data/users`);
const ApiError = require(`../middleware/utils/ApiError`);

const findUser = ({ username, password }) => {
	// Check credentials
  checkUserData(username, password);

  const user = users.find((user) => user.username === username);

	// Check if user exists
  if (!user) {
    throw ApiError.internal(`User with username ${username} is not found`);
  }
	
	// Check user's password
  if (user.password !== password) {
    throw ApiError.internal(`Unauthorized. Not correct password`);
  }

	return user;
};

const checkUserData = (username, password) => {
  // Check if username and password are in a request and they are not empty strings
  if (!username || !password) {
    throw ApiError.badRequest(
      `Parameters 'username', 'password' cannot be omitted`
    );
  }

  // Check if email parameter is type of string
  if (typeof username !== "string") {
    throw ApiError.badRequest(`Parameters 'username' have to be of type 'string'`);
  }

  // Check if email parameter is type of string
  if (typeof password !== "string") {
    throw ApiError.badRequest(
      `Parameters 'password' have to be of type 'string'`
    );
  }
};

const generateToken = user => {
	return jwt.sign(user, process.env.JWT_SECRET, {
    expiresIn: `30d`,
  });
}


module.exports = { findUser, checkUserData, generateToken};
