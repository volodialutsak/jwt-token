const jwt = require(`jsonwebtoken`);
const users = require(`../data/users`);
const ApiError = require(`../middleware/utils/ApiError`);

const getUsernames = () => {
  return users.map((user) => user.username);
};

const parseToken = (bearerToken) => {
  // Check if there is token
  if (!bearerToken) {
    throw ApiError.badRequest(`For this request token is required`);
  }

	return bearerToken.split(' ')[1];
};

const getUserData = (_id, token) => {
	const user = users.find(user => user.id = _id);

	// Check if user exists
  if (!user) {
    throw ApiError.internal(`User with id: ${_id} is not found`);
  }

	const decoded = jwt.verify(token, process.env.JWT_SECRET);
	const {id, name, username} = decoded;

	if (_id !== id) {
		throw ApiError.badRequest(`Unauthorized request. Bad token`);
	}
	
	return {id, name, username};
}

module.exports = { getUsernames, parseToken, getUserData };
