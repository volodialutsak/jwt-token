const ApiError = require('./utils/ApiError');

function apiErrorHandler(err, req, res, next) {     
    if (err instanceof ApiError) {
        console.log(err);
        res.status(err.code).json(err.message);
        return;
    }
    
    console.log(err);
    res.status(500).json('Server error');    
}

module.exports = apiErrorHandler;