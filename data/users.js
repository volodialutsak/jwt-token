const users = [
  {
  	name: 'Misha',
	username: 'misha@gmail.com',
	password: 'secret',
	id: '0'
  },
  {
  	name: 'Kolia',
	username: 'kolia@gmail.com',
	password: 'secret123',
	id: '1'
  },
  {
  	name: 'Olia',
	username: 'olia@gmail.com',
	password: 'secret456',
	id: '2'
  },
  {
  	name: 'Petia',
	username: 'petia@gmail.com',
	password: 'secret789',
	id: '3'
  },
  {
  	name: 'Vova',
	username: 'vova@gmail.com',
	password: 'top_secret',
	id: '4'
  },
	{
  	name: 'Masha',
	username: 'masha@gmail.com',
	password: 'the_most_secret',
	id: '5'
  },
  {
  	name: 'Lera',
	username: 'valeria@gmail.com',
	password: 'new_secret',
	id: '6'
  },
  {
    name: 'Maxim',
	username: 'maxim@gmail.com',
	password: 'Aa1234567',
	id: '7'
  },
  {
    name: 'Tania',
	username: 'tetiana@gmail.com',
	password: 'never_guess',
	id: '8'
  },
  {
    name: 'Kyrylo',
	username: 'kyrylo@gmail.com',
	password: 'dont_try',
	id: '9'
  },
];

module.exports = users;
